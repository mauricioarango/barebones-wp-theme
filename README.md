
# BAREBONES WP TEMPLATE

Basic WP template. No CSS, No JS.

This template is not ready for direct usage. Instead, depending on project needs, the php templates should be written. But it is useful in that it:

+ Does not have unwanted CSS
+ Does not have unwanted JS files
+ Presents a folder structure that has proven to work well in past projects
+ Has an npm, webpack set up (just needs to run npm i) to bundle JS, trans SASS into CSS and minify files.


## History
10-19-20: First commit