<?php
/**
* Single post template
*
* @package WordPress
* @version 1.0
*/
get_header();
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="single">

    <div class="container">
        <?php echo the_content(); ?>

        <?php
            # if in need to load custom templates according to a page type or page name
            //  # Gets template for single Proyectos page
            //  if ($post->post_type == 'proyectos'): silencio_partial('/templates/proyectos/proyectos-single', ['post'=>$post]);
            //  endif;
            // # Gets template for single Communities pages
            //  if ($post->post_type == 'compromisos'): silencio_partial('/templates/compromisos/compromisos-single', ['post'=>$post]);
            //  endif;
         ?>

    </div>
</div>
<?php endwhile; endif; ?>
<?php get_footer();