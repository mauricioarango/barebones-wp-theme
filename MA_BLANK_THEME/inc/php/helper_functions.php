<?php
/* *** *** ***
    Collection of different helper functions
    1. ma_is_child_of($page_id)   use to determine if current post is descendant of $page_id post

*** *** *** */



/* - - - - - - - - - - - - - - - - - - - - -
 1. Find if current page is child of a given page
    Takes the id of ancestor page
   - - - - - - - - - - - - - - - - - - - - */
function ma_is_child_of($page_id) {
    global $post;
    $is_child = false;
    $parents = get_post_ancestors($post);
    if ($parents) {
        foreach ($parents as $one_parent_id) {
            if ($one_parent_id == $page_id) {
                $is_child = true;
                break;
            }
        }
    }
    return $is_child;
};


/**
* Load a template part & pass in variables declared in caller scope. Optionally return as a string.
* @param string $path path to template file, minus .php (eg. `content-page`, `partial/folder/template-name`)
* @param array $args map of variables to load into scope
* @param bool $echo echo or return rendered template
* @return null or rendered template string
* as seen in:
* https://github.com/viastudio/Silencio/blob/master/functions.php
* https://viastudio.com/better-partial-templates-in-wordpress/
*/
function silencio_partial($path, $args = [], $echo = true) {
    if (!empty($args)) {
        extract($args);
    }

    if ($echo) {
        include(locate_template($path . '.php'));
        return;
    }

    ob_start();
    include(locate_template($path . '.php'));
    return ob_get_clean();
}

/* - - - - - - - - - - - - - - - - - - - - -
    Blank navigation
    Use it inside the theme to call a nav menu
   - - - - - - - - - - - - - - - - - - - - */
function ma_blank_nav($theme_location, $class)
{
    wp_nav_menu(
    array(
        'theme_location'  => $theme_location,
        'echo'            => true,
        'fallback_cb'     => false,
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'depth'           => 0,
        'walker'          => '',
        'menu_class'      => $class.'-holder',
        'container'       => 'div',
        'container_class' => $class

        )
    );
}

 ?>