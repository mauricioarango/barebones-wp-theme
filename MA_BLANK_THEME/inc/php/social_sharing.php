<?php
/*
    Builds a UL of social media shareable links.
 */
if (!function_exists('MA_share')):
    function MA_share($post_id = 0, $socialNetwork =array('pinterest', 'facebook', 'twitter', 'linkedin', 'email', ))
{

        $url          = get_permalink($post_id);
        $encoded      = urlencode($url);
        $title        = get_the_title($post_id);
        $titleEncoded = urlencode($title);
        $links        = array();
        //if ('pinterest' in $socialNetwork) $links[] =
        if ( in_array('twitter' ,$socialNetwork) ) $links['twitter']  = array(
                    'https://twitter.com/intent/tweet?url=' . $encoded,
                     'icon-twitter',
                     'Twitter',
                     'anchor-twitter');

        if ( in_array(  'facebook', $socialNetwork) ) $links['facebook']  = array(
                    'http://www.facebook.com/share.php?u=' . $encoded . '&title=' . $titleEncoded,
                    'icon-facebook',
                    'Facebook',
                    'anchor-facebook');

        if ( in_array('email', $socialNetwork) ) $links['email'] = array(
            sprintf(
                'mailto:?subject=%s&body=%s',
                "$title | ".get_bloginfo('name'),
                "This page has been recommended to you: $url"
            ),
            'icon-mail',
            'Email',
            'anchor-mail'
        );
        if ( in_array(  'linkedin', $socialNetwork) ) $links['linkedin']  = array(
                'https://www.linkedin.com/shareArticle?url=' . $encoded . '&mini=' .'true'. '&title=' . $titleEncoded. '&source=' . get_bloginfo( 'name' ),
                'icon-linkedin',
                'Linked In',
                'anchor-linkedin');

        ?>

  <ul class="share">

    <?php
        $keys = array_keys($links); // gets the name of each social network (the key in $links array)
        foreach ($socialNetwork as $network):
            if (in_array($network, $keys) && $network == 'pinterest'): ?>

              <li>
                <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
                <a data-pin-do="buttonBookmark" data-pin-custom="true" data-pin-save="false" href="https://www.pinterest.com/pin/create/button/"><span class="icon-pinterest"></span></a>
              </li>
    <?php else: ?>
                <li  class='<?php echo $links[$network][3] ?>'><a href="<?php echo $links[$network][0]; ?>" title="Share on <?php echo $links[$network][2]; ?>" target="_blank"><span class="<?php echo $links[$network][1]; ?>"></span></a></li>

    <?php
            endif;
        endforeach;
    ?>

  </ul>

<?php
}
endif;
?>