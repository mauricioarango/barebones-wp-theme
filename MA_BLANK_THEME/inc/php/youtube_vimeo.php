<?php
// FUNCTIONS TO GET YOUTUBE, VIMEO EMBED CODES AND THUMBNAILS.
// USES YOUTUBE, VIMEO URLS IN THE FORM:
//
//  YOUTUBE: https://www.youtube.com/watch?v=aGVU_1Udhrs
//  VIMEO: https://vimeo.com/242564489
//
// CREATES YOUTUBE VIDEO FROM A YOUTUBE VIDEO URL
function ma_get_youtube_video( $video_source ){
    $video_player_template = '<iframe id="youtube-video" width="560" height="315" src="%s" frameborder="0" allowfullscreen enablejsapi="true"></iframe>';
    $video_embed_url_template = 'https://www.youtube.com/embed/%s?rel=0&amp;showinfo=0';
    if( preg_match( '/\?v=(.*)$/', $video_source, $matches ) ) return sprintf( $video_player_template, sprintf( $video_embed_url_template,  trim( $matches[1] ) ) );
    else return 'error';
}
// GETS YOUTUBE THUMBNAIL FROM  A YOUTUBE VIDEO URL
function ma_get_youtube_thumbnail( $video_source ){
    $video_thumbnail_template = '<img src="http://i3.ytimg.com/vi/%s/mqdefault.jpg">';
    if( preg_match( '/\?v=(.*)$/', $video_source, $matches ) )
        return sprintf( $video_thumbnail_template, trim( $matches[1] ) );
    else if( preg_match( '/\/v\/(.*)$/', $video_sources, $matches ) )
        return sprintf( $video_thumbnail_template, trim( $matches[1] ) );
    else return "unhandled video thumbnail, video source: <pre>$video_source</pre>";
}

// CREATES VIMEO VIDEO FROM A VIMEO VIDEO URL
function ma_get_vimeo_video ( $video_source ){
    $video_player_template = '<iframe class="embed_video" width="560" height="315" src="%s" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    $video_embed_url_template = 'https://player.vimeo.com/video/%s?title=0&byline=0&portrait=0';

    if( preg_match( '/\.com\/(.*)$/', $video_source, $matches ) ) return sprintf( $video_player_template, sprintf( $video_embed_url_template,  trim( $matches[1] ) ) );
    else return 'error';
}

// GETS VIMEO THUMBNAIL FROM FROM A VIMEO VIDEO URL
function ma_get_vimeo_thumb($video_source, $size = 'large'){
    // Gets video id from video source url
    if( preg_match( '/\.com\/(.*)$/', $video_source, $matches ) ) {
        $id = trim( $matches[1] );
        $vimeo = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
        $image =  $vimeo[0]['thumbnail_'.$size];
        $video_thumbnail_template = '<img src="%s">';
        return sprintf($video_thumbnail_template, $image);
    }else{
        return "Unhandled video player; video source: <pre>$video_source</pre>";
    }
}

//  Gets the video URL and returns the embed code
function ma_video_embed ($url){
    // Determines what type of video it is (youtube or vimeo)
    // and gets the embed code
    if (stripos($url, 'youtube') !== false) {
        $result = ma_get_youtube_video($url);
    }else if (stripos($url, 'vimeo') !== false) {
        $result = ma_get_vimeo_video($url);
    }else{
        $result = 'error';
    }

    // returns result
    if ($result == 'error'){
        return "Unhandled video player; video source: <pre>$url</pre>";
    }else{
        return $result;
    }
}

//
?>