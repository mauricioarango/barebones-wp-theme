<?php
/* - - - - - - - - - - - - - - - - - - - - -
    OPTIMIZATIONS TO THE DOCUMENT'S HEAD
   - - - - - - - - - - - - - - - - - - - - */

#
# ACTIONS
#
# Adding titles to themes
function theme_slug_setup() {
    add_theme_support( 'title-tag' );
 }

#
# ADDS VIEWPORT INITIAL SET UP
if (!function_exists('ma_viewport_setup')):

   function ma_viewport_setup()
   {
       $viewport = '<meta name="viewport" content="width=device-width, initial-scale=1"/>' . "\n";
       echo $viewport;
   }
endif;
#
# Includes Favicons
#
# Generated at:
# https://www.favicon-generator.org/
#
if (!function_exists('ma_favicons')):

   function ma_favicons()
   {
       $favfolder   = get_template_directory_uri().'/dist/images/favicon/';
       $favicons = '<link rel="apple-touch-icon" sizes="57x57" href="'.$favfolder.'apple-icon-57x57.png">
                  <link rel="apple-touch-icon" sizes="60x60" href="'.$favfolder.'apple-icon-60x60.png">
                  <link rel="apple-touch-icon" sizes="72x72" href="'.$favfolder.'apple-icon-72x72.png">
                  <link rel="apple-touch-icon" sizes="76x76" href="'.$favfolder.'apple-icon-76x76.png">
                  <link rel="apple-touch-icon" sizes="114x114" href="'.$favfolder.'apple-icon-114x114.png">
                  <link rel="apple-touch-icon" sizes="120x120" href="'.$favfolder.'apple-icon-120x120.png">
                  <link rel="apple-touch-icon" sizes="144x144" href="'.$favfolder.'apple-icon-144x144.png">
                  <link rel="apple-touch-icon" sizes="152x152" href="'.$favfolder.'apple-icon-152x152.png">
                  <link rel="apple-touch-icon" sizes="180x180" href="'.$favfolder.'apple-icon-180x180.png">
                  <link rel="icon" type="image/png" sizes="192x192"  href="'.$favfolder.'android-icon-192x192.png">
                  <link rel="icon" type="image/png" sizes="32x32" href="'.$favfolder.'favicon-32x32.png">
                  <link rel="icon" type="image/png" sizes="96x96" href="'.$favfolder.'favicon-96x96.png">
                  <link rel="icon" type="image/png" sizes="16x16" href="'.$favfolder.'favicon-16x16.png">
                  <link rel="manifest" href="'.$favfolder.'manifest.json">
                  <meta name="msapplication-TileColor" content="#ffffff">
                  <meta name="msapplication-TileImage" content="'.$favfolder.'ms-icon-144x144.png">
                  <meta name="theme-color" content="#ffffff">';
       echo $favicons;
   }
endif;

#
# FILTERS
#
# Removes invalid rel attribute values in the categorylist
function ma_remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}
# remove WP version from scripts & css
function ma_extras_remove_wp_ver_css_js($src)
{
    if (strpos($src, 'ver=')) {
        $src = remove_query_arg('ver', $src);
    }

    return $src;
}
# remove WP version from RSS
function ma_remove_wp_ver_rss()
{
    return '';
}

/* - - - - - - - - - - - - - - - - - - - - -
    Disable emoji's,
    prevents loading of css and js emoji files
   - - - - - - - - - - - - - - - - - - - - */
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' );
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
     if ( is_array( $plugins ) ) {
     return array_diff( $plugins, array( 'wpemoji' ) );
     } else {
     return array();
     }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
     if ( 'dns-prefetch' == $relation_type ) {
     /** This filter is documented in wp-includes/formatting.php */
     $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

    $urls = array_diff( $urls, array( $emoji_svg_url ) );
     }

    return $urls;
}
/* - - - - - - - - - - - - - - - - - - - - -
    Removes Gutenberg Block Library CSS from loading on the frontend
    - the theme doesn't use guttemberg
   - - - - - - - - - - - - - - - - - - - - */
function smartwp_remove_wp_block_library_css(){
wp_dequeue_style( 'wp-block-library' );
wp_dequeue_style( 'wp-block-library-theme' );
}
/*  -  -  -  -  -  -  -  -  -  -  
    Prevent wp-embed from loading
    -  -  -  -  -  -  -  -  -  -  */
function my_deregister_scripts(){
    wp_deregister_script( 'wp-embed' );
}

# Add Acctions
add_action( 'after_setup_theme', 'theme_slug_setup' ); # add title support
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css' );
add_action('wp_head', 'ma_viewport_setup');
add_action('wp_head', 'ma_favicons');
add_action( 'init', 'disable_emojis' );
add_action( 'wp_enqueue_scripts', 'my_deregister_scripts' );


# Filters
add_filter('the_category', 'ma_remove_category_rel_from_category_list'); # Remove invalid rel attribute
add_filter('style_loader_src', 'ma_extras_remove_wp_ver_css_js', 9999); # remove WP version from css
add_filter('script_loader_src', 'ma_extras_remove_wp_ver_css_js', 9999); # remove Wp version from scripts
add_filter('the_generator', 'ma_remove_wp_ver_rss'); # remove WP version from RSS

# Remove Actions:
remove_action('wp_head', 'feed_links_extra', 3); # Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); # Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); # Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); # Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); # Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); # Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); # Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); # Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); # Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);



/*-------------------------------------
    TO DO:
    Review Google Analytics
---------------------------------------*/

# ADDS GOOGLE ANALYTICS
if (!function_exists('ma_addgoogleanalytics')):
    function ma_GA_ID(){
      $id = "";
      $GA_enabled = get_field('enable_google_analytics', 'option');
      if ($GA_enabled){
        $GA_id = get_field('google_analytics_id', 'option');
        #echo 'THE ID IS: '.$GA_id.'~~~';
        if (!empty($GA_id)){
          $id = $GA_id;
        }
      }
      return $id;
    }

    function ma_addgoogleanalytics( ){
        # Gets GA ID
        $id = ma_GA_ID();
        if ( empty($id)) return false;

         $ga = "<script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https:#www.google-analytics.com/analytics.js','ga');
              ga('create', '".$id."', 'auto');
              ga('send', 'pageview');
            </script>" . "\n\n";

        # If ad tracking is also necessary -otherwise comment it out
        # $ga .= "<script type='text/javascript'>
        #         var _gaq = _gaq || [];
        #         _gaq.push(['_setAccount', '".$id."']);
        #         _gaq.push(['_trackPageview']);
        #         (function() {
        #           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        #           ga.src = ('https:' == document.location.protocol ? 'https:#ssl' : 'http:#www') + '.google-analytics.com/ga.js';
        #           (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
        #         })();
        #       </script>" . "\n\n";

        echo $ga;
    }

    add_action('wp_head', 'ma_addgoogleanalytics');

endif;