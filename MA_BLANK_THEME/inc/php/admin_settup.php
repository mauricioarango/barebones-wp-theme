<?php
/*-------------------------------------
    admin_settup.php

    Custom configuration for admin screens
---------------------------------------*/
/**
 * Remove customizer options.
 *
 * @since 1.0.0
 * @param object $wp_customize
 */

/*-------------------------------------
 Removes the customizer menu in Wp's dashboard
---------------------------------------*/
function ma_remove_customizer_menu()
{
    remove_submenu_page( 'themes.php', 'customize.php?return=' . urlencode($_SERVER['SCRIPT_NAME']));
}

/*-------------------------------------
 Disables default dashboard widgets
 ---------------------------------------*/
function disable_default_dashboard_widgets() {

    remove_meta_box('dashboard_right_now', 'dashboard', 'core');
    remove_meta_box('dashboard_activity', 'dashboard', 'core');
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');
    remove_meta_box('dashboard_plugins', 'dashboard', 'core');

    remove_meta_box('dashboard_quick_press', 'dashboard', 'core');
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');
    remove_meta_box('dashboard_primary', 'dashboard', 'core');
    remove_meta_box('dashboard_secondary', 'dashboard', 'core');
}

/*-------------------------------------
    REMOVES COMMENTS EVERYWHERE
---------------------------------------*/
// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
    $post_types = get_post_types();
    foreach ($post_types as $post_type) {
        if(post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
}

// Close comments on the front-end
function df_disable_comments_status() {
    return false;
}

function df_disable_comments_hide_existing_comments($comments) {
    $comments = array();
    return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);// Hide existing comments

// Remove comments page in menu
function df_disable_comments_admin_menu() {
    remove_menu_page('edit-comments.php');
}

// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
    global $pagenow;
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url()); exit;
    }
}

// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}

// Remove comments links from admin bar
function df_disable_comments_admin_bar($wp_adminbar) {
    $wp_adminbar->remove_menu('comments');
}

/*-------------------------------------
Moves Media Link to the top on the admin's side menu
---------------------------------------*/
function wpse_233129_custom_menu_order() {
    return array( 'index.php', 'upload.php' );
}

/* - - - - - - - - - - - - - - - - - - - - -
    REMOVES THE '+ NEW' MENU FROM TOOLBAR
   - - - - - - - - - - - - - - - - - - - - */
function wpse_260669_remove_new_content(){
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'new-content' );
}
add_action( 'wp_before_admin_bar_render', 'wpse_260669_remove_new_content' );


/*-------------------------------------
  CUSTOM CSS FOR ADMIN:
---------------------------------------*/
// Add google font to use in back end
function my_custom_admin_head() {
    echo '<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans&family=Inter&display=swap" rel="stylesheet">
';
}

function ma_acf_admin_enqueue_scripts() {
    // register style
    wp_register_style('styleAdminACF', get_stylesheet_directory_uri().'/inc/css/ma_acf.css', false, false, 'all'); // For ACF
    wp_enqueue_style( 'styleAdminACF' );
}
function ma_enqueue_custom_admin_style(){
    wp_register_style('styleAdmin', get_stylesheet_directory_uri().'/inc/css/ma_admin.css', false, false, 'all'); // For overal admin
    wp_enqueue_style( 'styleAdmin' );
}

/* - - - - - - - - - - - - - - - - - - - - -
    Forces paste as plain text in the tiny mce editor
   - - - - - - - - - - - - - - - - - - - - */
function ag_tinymce_paste_as_text( $init ) {
    $init['paste_as_text'] = true;
    return $init;
}

/*-------------------------------------
    Addition of W & H to media table
    and improvement on WP's compression quality
---------------------------------------*/
function wh_column( $cols ) {
    $cols["dimensions"] = "Dimensions (w x h)";
    return $cols;
}
function wh_value( $column_name, $id ) {
    $meta = wp_get_attachment_metadata($id);
           if(isset($meta['width']))
           echo $meta['width'].' x '.$meta['height'];
}


/*-------------------------------------
  ACF JSON: Creates a custom saving and loading point (folder) for ACF to look in
            IN the online sites all the information will be read from the acf_json,
            so there's no need to use ACF's backend's interface.
---------------------------------------*/
function my_acf_json_load_point( $paths ) {

    unset($paths[0]); # remove original path (optional)

    $paths[] = get_stylesheet_directory() . '/inc/acf-json'; # append path

    return $paths; # return
}
 function my_acf_json_save_point( $path ) {

    $path = get_stylesheet_directory() . '/inc/acf-json'; # update path

    return $path; # return
 }

/*-------------------------------------
    EXECUTES ACTIONS AND FILTERS
---------------------------------------*/

/*
  * Let WordPress manage the document title.
  * By adding theme support, we declare that this theme does not use a
  * hard-coded <title> tag in the document head, and expect WordPress to
  * provide it for us.
  */
add_theme_support( 'title-tag' );

add_filter( 'custom_menu_order', '__return_true' ); # Moves Media Link to the top on the admin's side menu
add_filter( 'menu_order', 'wpse_233129_custom_menu_order' ); #Moves Media Link to the top on the admin's side menu
# comments related
add_action('admin_init', 'df_disable_comments_post_types_support');# Disable support for comments and trackbacks in post types
add_filter('comments_open', 'df_disable_comments_status', 20, 2);# Close comments on the front-end
add_filter('pings_open', 'df_disable_comments_status', 20, 2);# Close comments on the front-end
add_action('admin_menu', 'df_disable_comments_admin_menu');# Remove comments page in menu
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');# Redirect any user trying to access comments page
add_action('admin_init', 'df_disable_comments_dashboard');# Remove comments metabox from dashboard
add_action('admin_bar_menu', 'df_disable_comments_admin_bar', 999);# Remove comments links from admin bar
# others
add_action( 'admin_menu', 'ma_remove_customizer_menu' ); # Removes the theme customizer.
add_action('admin_menu', 'disable_default_dashboard_widgets'); # Disables dashboard's widgets

add_action( 'admin_head', 'my_custom_admin_head' ); # adds custom content to admin head
add_action( 'acf/input/admin_enqueue_scripts', 'ma_acf_admin_enqueue_scripts' ); #CUSTOM CSS FOR WHEN EDITING A POST USING ACF
add_action( 'admin_enqueue_scripts', 'ma_enqueue_custom_admin_style' ); # adds css for all admin screens

# Media
add_filter( 'manage_media_columns', 'wh_column' ); # Ads Width to media table
add_action( 'manage_media_custom_column', 'wh_value', 10, 2 ); # Ads Height to media table
add_filter('jpeg_quality', function(){return 90 ;}); # Changes WP's DEFAULT IMAGE COMPRESSION -SINCE WP 4.5 IT'S 82%

# ACF
add_filter('acf/settings/load_json', 'my_acf_json_load_point'); # adds folder to load and save ACF ADMIN JSON
add_filter('acf/settings/save_json', 'my_acf_json_save_point');

# Tiny MCE
add_filter('tiny_mce_before_init', 'ag_tinymce_paste_as_text'); # Forces paste as text

# Enable excerpt in pages
add_post_type_support( 'page', 'excerpt' );

# Removes Gutenberg Editor:
add_filter('use_block_editor_for_post', '__return_false', 10);
?>