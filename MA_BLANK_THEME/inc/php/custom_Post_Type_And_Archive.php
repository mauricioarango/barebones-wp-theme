<?php
/* - - - - - - - - - - - - - - - - - - - - -
    CREATION OF CUSTOM TAXONOMIES
    & CUSTOM POST TYPES
   - - - - - - - - - - - - - - - - - - - - */

/* - - - - - - - - - - - - - - - - - - - - -
    Creates custom taxonomies.
    Should always come before the CPT creation
   - - - - - - - - - - - - - - - - - - - - */

function ma_create_custom_post_taxonomies() {

    $taxonomies_to_create = array();
    # Create arrays of key info for each custom taxonomy
    # Example
    /*
            $arquitectos_orig = array(
                'tax_slug'  => 'origen',
                'cpt'       => 'arquitectos',
                'hierarchical'  => true,
                'name'          => 'Origen',
                'singular_name' => 'Origen',
                'search_items'  => 'Buscar Origen',
                'edit_item'     => 'Editar Origen',
                'add_new_item'  => 'Añadir Nuevo Origen'
            );
            # Add to taxonomies array
            array_push($taxonomies_to_create, $arquitectos_orig);
    */

    # Loosp through bundled array and creates custom taxomoies
    foreach ($taxonomies_to_create as $tax):

        $tax_slug       = $tax['tax_slug'];
        $cpt            = $tax['cpt'];
        $hierarchical   = $tax['hierarchical'];
        $name           = $tax['name'];
        $singular_name  = $tax['singular_name'];
        $search_items   = $tax['search_items'];
        $edit_item      = $tax['edit_item'];
        $add_new        = $tax['add_new_item'];

        register_taxonomy(
            $tax_slug,
            $cpt,
            array(
                'labels' => array(
                   'name'          => $name ,
                   'singular_name' => $singular_name ,
                   'search_items'  => $search_items,
                   'edit_item'     => $edit_item,
                   'add_new_item'  => $add_new
                ),
                'hierarchical' => $hierarchical,
                'query_var'    => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'meta_box_cb' => false, # hides the side metabox
                'show_admin_column' => true,
                'show_in_rest' => false,
                'rewrite'      => array(
                    'slug' => $tax_slug,
                    'with_front' => false
                 )
        ));
    endforeach;

}
add_action( 'init', 'ma_create_custom_post_taxonomies',0 );

/* - - - - - - - - - - - - - - - - - - - - -
    Creates custom posttypes
   - - - - - - - - - - - - - - - - - - - - */

function ma_create_post_type() {

    # Bundle of CPTs
    $cpt_arr = array( );

    # Individual CPT to create:
    # ... create as many arrays as custom post types are needed
    # ... taxonomies are registered with ma_create_custom_post_taxonomies()
    # Example
    /*
        #  architects custom post type
        $arq = array(
            'plural' => 'Architects',
            'singular' => 'Arquitect',
            'slug' => 'architects',
            'hierarchical' => false
        );
        # push to cpt_arr
        array_push($cpt_arr, $arq);
    */
    # Registers each of the CPT
    foreach ($cpt_arr as $cpt):
        register_post_type( $cpt['slug'],
            array(
                'labels' => array(
                    'name' => $cpt['plural'],
                    'singular_name' => $cpt['singular']
                ),
            'public' => true,
            'has_archive' => true,
            'hierarchical' => $cpt['hierarchical'],
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'show_in_menu' => true,
            "menu_position" => 5,
            'show_in_rest' => true,
            'query_var' => true,
            'rewrite' => array('slug' => $cpt['slug']),
            'supports' =>array('title','editor', 'custom-fields','thumbnail',
            'page-attributes')
            )
        );
    endforeach;

}
add_action( 'init', 'ma_create_post_type', 1 );
?>