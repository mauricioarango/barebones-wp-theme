<?php
/*-------------------------------------
    Set of functions to load responsive and lazy images in different scenarios

    Also adds functions to:
    1. control WP image compression
    2. Add W & H to WP's media table

---------------------------------------*/



/* - - - - - - - - - - - - - - - - - - - - -
    SINGLE LAZY IMAGE WITH srcset attribute

   - - - - - - - - - - - - - - - - - - - - */
if (! function_exists('ma_image_lazy_responsive')):
    # $img_id -> id of img to get
    # $sizes: FOR NOW: string with 2 (ONLY 2!) images sizes, first the mobile, second the larger version,  to get.
    # $fig: boolean, whether or not to output the tag inside a figure tag
    # Example:
    # $sizes = 'about-sm, about-lg';
    # As a rule, use image sizes that are 2x to 3x larger than required width.
    # Full width in iphoneX is 375px. accomodating for screen resolution it would be 730 (@ 2x) or 1125 (@3x)
    function ma_image_lazy_responsive($img_id, $sizes, $fig = false){
        # Gets img caption

        $img_caption = get_the_title($img_id);
        # converts $sizes to an array
        $sizes_arr = explode(",", $sizes );
        # removes any unwated blank spaces
        $sizes_arr = array_map('trim',$sizes_arr);
        # print_r($sizes_arr);

        $data_srcset = '';
        $sizes_attribute = "(max-width: 760px) ";

        $length = count($sizes_arr);
        $i = 0;
        foreach ($sizes_arr as $size_single):
            $img_array = wp_get_attachment_image_src( $img_id, $size_single );
            if ($i == $length - 1 ){
                $data_srcset .= $img_array[0].' '.$img_array[1].'w';
                $sizes_attribute .= $img_array[1].'px';
            }else{
                $data_srcset .= $img_array[0].' '.$img_array[1].'w,'."\n";
                $sizes_attribute .= $img_array[1].'px,'."\n";
            }
            $i++;
        endforeach;
        # removes left out coma at the end
        $data_srcset = rtrim($data_srcset, ', ');
        $sizes_attribute = rtrim($sizes_attribute, ', ');
        if ( !$fig ):
        $img = '<img alt="'.$img_caption.'"
                    sizes="'.$sizes_attribute.'"
                    data-srcset="'.$data_srcset.'"
                    data-src="'.$img_array[0].'"
                    class="lazyload">';
        else:
            $img = '<figure>
                    <img alt="'.$img_caption.'"
                        sizes="'.$sizes_attribute.'"
                        data-srcset="'.$data_srcset.'"
                        data-src="'.$img_array[0].'"
                        class="lazyload" >
                    <figcaption>'.
                        $img_caption.
                   '</figcaption></figure>';
        endif;
        return $img;

    }
endif;

/* - - - - - - - - - - - - - - - - - - - - -
SINGLE LAZY IMAGE WITH srcset attribute
-- use when w and h in the 2 sources doesn't change.

   - - - - - - - - - - - - - - - - - - - - */
if (! function_exists('ma_image_lazy_responsive_fixed_ratio')):
    # $img_id -> id of img to get
    # $sizes: FOR NOW: string with 2 (ONLY 2!) images sizes, first the mobile, second the larger version,  to get.
    # Example:
    # $sizes = 'about-sm, about-lg';
    # As a rule, use image sizes that are 2x to 3x larger than required width.
    # Full width in iphoneX is 375px. accomodating for screen resolution it would be 730 (@ 2x) or 1125 (@3x)
    function ma_image_lazy_responsive_fixed_ratio($img_id, $sizes, $fig = false ){
        # Gets img caption
        $img_caption = get_the_title($img_id);
        # converts $sizes to an array
        $sizes_arr = explode(",", $sizes );
        # removes any unwated blank spaces
        $sizes_arr = array_map('trim',$sizes_arr);
        # print_r($sizes_arr);

        $data_srcset = '';
        $data_ratio = '';
        $sizes_attribute = "(max-width: 760px) ";

        foreach ($sizes_arr as $size_single):
            $img_array = wp_get_attachment_image_src( $img_id, $size_single );
            $data_srcset .= $img_array[0].' '.$img_array[1].'w'.', ';
            $sizes_attribute .= $img_array[1]."px, ";
            $data_ratio = round($img_array[2] / $img_array[1], 1);
        endforeach;

        # removes left out coma at the end
        $data_srcset = rtrim($data_srcset, ', ');
        $sizes_attribute = rtrim($sizes_attribute, ', ');
        $ratioPercent = $data_ratio * 100;
        $ratioPercent .= '%';

        if ( !$fig ):

            $img = '<div class="lazyload_fixed_ratio" style="padding-bottom:'.$ratioPercent.'">';
            $img .= '<img   class="lazyload"
                            data-sizes="auto"
                            data-ratio="'.$data_ratio.'"
                            data-srcset="'.$data_srcset.'"
                            sizes="'.$sizes_attribute.'"
                            data-src="'.$img_array[0].'"
                            alt="'.$img_caption.'">';
            $img .= '</div>';

        else:
            $img = '<figure class="lazyload_fixed_ratio">
                    <img    class="lazyload"
                            data-sizes="auto"
                            data-ratio="'.$data_ratio.'"
                            data-srcset="'.$data_srcset.'"
                            sizes="'.$sizes_attribute.'"
                            data-src="'.$img_array[0].'"
                            alt="'.$img_caption.'">
                    <figcaption>'.
                        $img_caption.
                   '</figcaption></figure>';
        endif;

        return $img;
    }
endif;

/* - - - - - - - - - - - - - - - - - - - - -
    FEATURED LAZY IMAGE WITH srcset attribute

   - - - - - - - - - - - - - - - - - - - - */
if (! function_exists('ma_featured_lazy_responsive')):
    # $post_id -> id of post containing featured img
    # $sizes: FOR NOW: string with 2 (ONLY 2!) images sizes, first the mobile, second the larger version,  to get.
    # Example:
    # $sizes = 'about-sm, about-lg';
    # As a rule, use image sizes that are 2x to 3x larger than required width.
    # Full width in iphoneX is 375px. accomodating for screen resolution it would be 730 (@ 2x) or 1125 (@3x)
    function ma_featured_lazy_responsive($post_id, $sizes= 'full', $fig = false){
        $img_id = get_post_thumbnail_id($post_id);

        $noImageText = 'No hay una imagen destacada.';

        if ( !isset($img_id) || $img_id == 0){
            if ($fig){
                $noImage = '<figure><figcaption>'.__($noImageText, 'ma_custom_strings').'</figcaption></figure>';
            }else{
                $noImage = __($noImageText, 'ma_custom_strings');
            }
            return $noImage;
        }else{
            # Once we have the image id we can call ma_image_lazy_responsive
            $img = ma_image_lazy_responsive($img_id, $sizes, $fig);
            return $img;
         }
    }
endif;

/* - - - - - - - - - - - - - - - - - - - - -
    RESPONSIVE AND LAZY BACKGROUND IMAGE
    RETURNS attributes to be added to template tag that needs the background image

   - - - - - - - - - - - - - - - - - - - - */
if (! function_exists('ma_background_responsive_lazy')):
    # $img_id -> id of img to get
    # $sizes: FOR NOW: string with 2 (ONLY 2!) images sizes, first the mobile, second the larger version,  to get.
    # Example:
    # $sizes = 'about-sm, about-lg';
    # As a rule, use image sizes that are 2x to 3x larger than required width.
    # Full width in iphoneX is 375px. accomodating for screen resolution it would be 730 (@ 2x) or 1125 (@3x)
    function ma_background_responsive_lazy($img_id, $sizes){

        $sizes_arr = explode(",", $sizes );
        # removes any unwated blank spaces
        $sizes_arr = array_map('trim',$sizes_arr);
        $data_srcset = 'data-bgset="';
        $i = 0;
        foreach ($sizes_arr as $size_single):
            $img_array = wp_get_attachment_image_src( $img_id, $size_single );

            if ($i == 0):
                $data_srcset .= $img_array[0].' '.$img_array[1].'w, ';
            elseif ($i == 1):
                $data_srcset .= $img_array[0].' '.$img_array[1].'w"';
            endif;

            $i++;

        endforeach;

        return $data_srcset;

        }
endif;

/* - - - - - - - - - - - - - - - - - - - - -
    RETURNS An HTML5 VIDEO
   - - - - - - - - - - - - - - - - - - - - */
   if (! function_exists('ma_videoHTML5')):
       # $vid_ID = number, id of video file to get
       # $videoAttributes = string, list of attributes to use in video tag
       function ma_videoHTML5($vid_ID, $videoAttributes){

          $video = get_post($vid_ID);
          $video_meta = wp_get_attachment_metadata( $video->ID);
          $poster =  get_the_post_thumbnail_url($video, 'full');
          $posterImage = '';
          if ($poster) $posterImage = 'poster="'.$poster.'"';

          $videoHTMl = '';
          if ($video != null):
            $videoHTML = '<figure>'.
                         '<video '.$videoAttributes.' width="'.$video_meta['width'].'" height="'.$video_meta['height'].'" '.$posterImage.'>'.
                             '<source src="'.$video->guid.'" '.
                             'type="'.$video->post_mime_type.'">'.
                             '<p class="video-error">'.
                             __('Su navegador no soporta este tipo de videos', 'ma_custom_strings').
                             '</p>'.
                             '</video>'.
                        '<figcaption>'.
                            '<h4>'.$video->post_title.'</h4>'.
                            '<p>'.$video->post_excerpt.'</p>'.
                        '</figcaption>'.
                        '</figure>';

          endif;

          return $videoHTML;

       }
   endif;


?>