<!-- pagination -->
<div class="pagination">
    <!-- Add the pagination functions here. -->
    <div class="nav-previous alignleft"><?php previous_posts_link( 'Older posts' ); ?></div>
    <div class="nav-next alignright"><?php next_posts_link( 'Newer posts' ); ?></div>
</div>
<!-- /pagination -->
