<?php
/* - - - - - - - - - - - - - - - - - - - - -
    textAndImage.php

    master component for text and image layouts

    $row: array, all the values for current row
    $sizes: string, comma sep. list of sizes to use
   - - - - - - - - - - - - - - - - - - - - */
 ?>
<?php
    // assigned variables
    $classBase = 'textAndImage';

    $imagen_a_la_izquierda = $row[ 'imagen_a_la_izquierda' ];
    $imagen = $row[ 'imagen' ];
    $titulo_principal = $row[ 'titulo_principal' ];
    $titulo_secundario = $row[ 'titulo_secundario' ];
    // $encabezado_del_texto_principal = $row[ 'encabezado_del_texto_principal' ];
    $texto_editorial = $row[ 'texto_editorial' ];
    $quote = $row[ 'cita' ];
    $author =  $row[ 'autor_de_la_cita' ];
    $imagen_a_la_izquierda ?  $alignClass = $classBase.'--left' :  $alignClass = $classBase.'--right';
    $classImage = ''; # used to define if the image needs to be top aligned or not
 ?>

<!-- .<?php echo $classBase; ?>-->
<div class="<?php echo $classBase.' '.$alignClass; ?>">

    <?php if ( !empty($titulo_secundario) || !empty($titulo_principal) ): ?>

        <!-- .<?php echo $classBase; ?>-header -->
        <header class="<?php echo $classBase; ?>-header header-withSubs">
            <?php if( !empty($titulo_secundario)): ?>
                <h3><?php echo $titulo_secundario; ?></h3>
            <?php endif; ?>
            <?php if( !empty($titulo_principal)): ?>
                <h2><?php echo $titulo_principal; ?></h2>
            <?php endif; ?>
        </header>
        <!-- /.<?php echo $classBase; ?>-header -->
    <?php else:
        # sinde there's no header the image should be top aligned
        $classImage = 'topAligned';
    endif; ?>




                <?php if ( !empty($quote) ): ?>

                    <div class="<?php echo $classBase; ?>-quote">
                        <blockquote>
                            <?php echo $quote ?>
                            <?php if( !empty($author) ): ?>
                                <div class="<?php echo $classBase; ?>-quote-author">
                                    <?php echo $author; ?>
                                </div>
                            <?php endif; ?>
                        </blockquote>
                    </div>

                <?php endif; ?>

                <?php if( !empty($texto_editorial)): ?>
                <div class='<?php echo $classBase; ?>-desc'>
                  <?php echo $texto_editorial; ?>
                </div> <!-- .<?php echo $classBase; ?>-contents-copy-desc -->
                <?php endif; ?>



            <?php if ( !empty($imagen) ): ?>
            <div class="<?php echo  $classImage.' '.$classBase; ?>-image">
                    <?php echo ma_image_lazy_responsive($imagen, $sizes, true) ?>
            </div>
            <?php endif; ?>


</div>
 <!-- /.<?php echo $classBase; ?>-->