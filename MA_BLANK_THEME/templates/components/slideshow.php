<?php
/* - - - - - - - - - - - - - - - - - - - - -
    slideshow.php

    master component for slideshows

    $images = array with image IDs
    $class  = string class to use
    $sizes = string 'size1, size2';
    $id  = string 'id' of carousel
   - - - - - - - - - - - - - - - - - - - - */
 ?>
 <?php
 $stylesheet_directory_path = get_stylesheet_directory();

 if ( !isset($id) ) $id = 'slider-'.rand();
 ?>
<!-- .swiper-container -->
 <div class="swiper-container <?php echo $class; ?>" id='<?php echo $id; ?>'>

    <div class="swiper-wrapper">

        <?php foreach ($images as $img): ?>

            <div class="swiper-slide">
                <?php echo ma_image_lazy_responsive($img, $sizes, true) ?>
            </div>

        <?php endforeach; ?>

    </div>
    <!-- /.swiper-wrapper -->

    <?php # If we need pagination ?>
    <div class="swiper-pagination"></div>
 </div>
 <!-- /.swiper-container -->

    <?php # If we need navigation buttons ?>
    <div class="swiper-button swiper-button-prev <?php echo $id ?>-swiper-button-prev"><?php echo file_get_contents($stylesheet_directory_path."/dist/images/svg/arrow_left.svg"); ?></div>
    <div class="swiper-button swiper-button-next <?php echo $id ?>-swiper-button-next"><?php echo file_get_contents($stylesheet_directory_path."/dist/images/svg/arrow_right.svg"); ?></div>
