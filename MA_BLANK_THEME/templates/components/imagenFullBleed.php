<?php
/* - - - - - - - - - - - - - - - - - - - - -
    imagenFullBleed.php

    master component for full Bleed images

    $classAside = string, class to use in aside
    $sizes  = string, comma separated list of sizes
    $parallax = boolean, whether this is a parallax block or not
    $image = number, image id
   - - - - - - - - - - - - - - - - - - - - */
 ?>

 <aside class="<?php echo $classAside;?> flex--fullBleed">

     <?php
         if ($parallax):
            silencio_partial('/templates/components/imageParallax', ['image' => $image, 'sizes' => $sizes]);
         else:
             echo ma_image_lazy_responsive($image, $sizes, true);
         endif;
      ?>

 </aside>