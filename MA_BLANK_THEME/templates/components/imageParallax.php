<?php
/* - - - - - - - - - - - - - - - - - - - - -
    imageParallax.php

    master component for parallax images

    $imgage = number, image id
    $sizes  = string, comma separated list of sizes
   - - - - - - - - - - - - - - - - - - - - */
 ?>
 <!-- .imageParallax -->
 <div class="imageParallax lazyload" <?php echo ma_background_responsive_lazy($image, $sizes); ?> ></div>
 <!-- /.imageParallax -->