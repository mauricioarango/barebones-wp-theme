<?php
/* - - - - - - - - - - - - - - - - - - - - -
    <?php echo $baseClass; ?>.php

    master component for text and  2-column image layouts

    $row: array, all the values for current row
    $sizes: string, comma sep. list of sizes to use
   - - - - - - - - - - - - - - - - - - - - */
 ?>
<?php
# assigned variables

$texto_editorial = $row[ 'texto_editorial' ];
$imagenes_columna_ancha_ids = $row[ 'imagenes_columna_ancha' ];
$imagenes_columna_angosta_ids = $row[ 'imagenes_columna_angosta' ];

$baseClass = 'textAnd2column'
?>

<!-- .<?php echo $baseClass; ?>-->
<div class="<?php echo $baseClass; ?>">



    <!-- .<?php echo $baseClass; ?>-contents -->
    <div class="<?php echo $baseClass; ?>-contents">

            <?php #images only colomn ?>
            <div class="<?php echo $baseClass; ?>-contents-columnImages">

                <?php if (!empty($imagenes_columna_ancha_ids)): ?>
                <div class="<?php echo $baseClass; ?>-contents-columnImages-images">
                    <?php foreach($imagenes_columna_ancha_ids as $img): ?>
                       <?php echo ma_image_lazy_responsive($img, $sizes, true) ?>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>

            </div>
            <!-- /.<?php echo $baseClass; ?>-contents-columnImages -->

            <?php #copy colomn ?>
            <div class="<?php echo $baseClass; ?>-contents-copy">

                <?php if( !empty($texto_editorial)): ?>
                <div class='<?php echo $baseClass; ?>-contents-copy-desc'>
                  <?php echo $texto_editorial; ?>
                </div> <!-- .<?php echo $baseClass; ?>-contents-copy-desc -->
                <?php endif; ?>

                <?php if (!empty($imagenes_columna_angosta_ids)): ?>
                <div class="<?php echo $baseClass; ?>-contents-copy-images">
                   <?php foreach($imagenes_columna_angosta_ids as $img): ?>
                       <?php echo ma_image_lazy_responsive($img, $sizes, true) ?>
                   <?php endforeach; ?>
                </div>
                <?php endif; ?>

            </div>
            <!-- /.<?php echo $baseClass; ?>-contents-copy -->

    </div>
    <!-- ./<?php echo $baseClass; ?>-contents -->

</div>
 <!-- /.<?php echo $baseClass; ?>-->