<?php
/* - - - - - - - - - - - - - - - - - - - - -
    scrollDown.php

    Scroll Down tip and copy

    $targetClassName = target to scroll to
   - - - - - - - - - - - - - - - - - - - - */
   if ( !isset($targetClassName) ) $targetClassName = 'scrollTarget';
 ?>
<button class='scrollIndicator' data-target="<?php echo $targetClassName; ?>">

     <span><?php _e('desplazar', 'ma_custom_strings'); ?></span>

     <div class='scrollIndicator-icon'>
         <?php echo file_get_contents(get_stylesheet_directory()."/dist/images/svg/down_arrow.svg"); ?>
     </div> <!-- .header-scrollIndicator-icon -->

</button> <!-- .header-scrollIndicator -->

<?php #always keep the 'scroll here' after the button, otherwise js will fail ?>
<div class='scrollHere <?php echo $targetClassName; ?>'></div> <!-- .header-scrollIndicator-scrollHere -->