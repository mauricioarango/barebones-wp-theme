<?php
/* - - - - - - - - - - - - - - - - - - - - -
    form_image.php

    master component for form with an image

    $titulo = string, titulo_del_formulario,
    $texto = string, texto_formulario,
    $imagen = number, id de la imagen,
    $lass => string, class to use in divs as a prefix.
    $use => 'contactForm' --default value. Or 'careersForm'. Or 'individualProjectForm'
    $pageID => 'id' only used when getting a project page form
   - - - - - - - - - - - - - - - - - - - - */
 ?>
 <?php

    $stylesheet_directory_path = get_stylesheet_directory();
    # general phone
    $contact = get_page_by_path('informacion-de-contacto'); # contact page id
    $int_code = get_field( 'codigo_telefonico_ecuador', $contact->ID );
    $telephoneHeader = get_field('telefono_cabecera', $contact->ID); #gets the phone #
    $telephoneClean = str_replace(' ', '', $telephoneHeader);

    # gets the required type of form
    if (!isset($use)) {$use = 'contactForm';}

    $use = $use ?? 'contactForm';

    if ( $use == 'contactForm' ):
      // Gets the general contact form in the proper language
      if (ICL_LANGUAGE_CODE == 'es'):
        $form = get_field( 'formulario_general_de_contacto_espanol', $contact->ID );
      elseif (ICL_LANGUAGE_CODE == 'en'):
        $form = get_field( 'formulario_general_de_contacto_ingles', $contact->ID );
      endif;

    elseif ( $use == 'careersForm' ):
      // Gets the careers  form in the proper language
      if (ICL_LANGUAGE_CODE == 'es'):
        $form = get_field( 'forma_de_carreras_formulario_general_de_contacto_espanol', $contact->ID );
      elseif (ICL_LANGUAGE_CODE == 'en'):
        $form = get_field( 'forma_de_carreras_formulario_general_de_contacto_ingles', $contact->ID );
      endif;
    elseif ( $use == 'individualProjectForm' ):

      // Gets the careers  form in the proper language
      if (ICL_LANGUAGE_CODE == 'es'):
        $form = get_field( 'codigo_formulario_proyecto_--_projects_form_code_espanol', $pageID );
      elseif (ICL_LANGUAGE_CODE == 'en'):
        // first we get the id in spanish (the forms are stored in the spanish version)
        $spanishID = apply_filters( 'wpml_object_id', $pageID, 'proyectos', false, 'es'  );
        $form = get_field( 'codigo_formulario_proyecto_--_projects_form_code_ingles', $spanishID );
      endif;
      // $post->ID;

    endif;
 ?>
 <div class="<?php echo $class; ?> formHtml">
<?php
echo '<pre>';
  print_r($post);
echo '</pre>';

 ?>
     <div class='<?php echo $class; ?>-copy copy'>

      <div class='<?php echo $class; ?>-copy-side1 copy-side1'>
         <?php #  title ?>
         <?php if($titulo): ?>
           <h2><?php echo $titulo; ?></h2>
         <?php endif; ?>

         <?php #texto_formulario ?>
         <?php if ($texto): ?>
           <div class='<?php echo $class; ?>-copy-desc copy-desc'>
               <?php echo $texto; ?>

               <address>
                    <a href="tel:0<?php echo $telephoneClean; ?>" title="<?php echo _e('Llámenos', 'ma_custom_strings'); ?>">
                        <?php echo file_get_contents($stylesheet_directory_path."/dist/images/svg/phone.svg"); ?>
                        <span><?php echo $int_code.' '.$telephoneHeader; ?></span>
                    </a>
                </address>
           </div> <!-- .<?php echo $class; ?>-copy-desc -->
         <?php endif; ?>

                <?php
                 # in screenreaders the button to show form is not necessary because the form is not removed
                 ?>
               <div class="<?php echo $class;?>-copy-showButton button--showForm" aria-hidden='true' id="<?php echo $use; ?>">
                    <button class="buttonLike showForm" data-targetItem="<?php echo $class; ?>-copy-form" data-scrollTarget="scrollHere--<?php echo $use;?>">
                        <span><?php _e('CONTACTANOS', 'ma_custom_strings'); ?></span>
                    </button>
                    <div class="scrollHere" id="scrollHere--<?php echo $use;?>"></div>
                </div>

         <?php # form ?>

      </div>

      <div class='<?php echo $class; ?>-copy-side2 copy-side2'>

         <div class='<?php echo $class; ?>-copy-form copy-form'>

            <?php echo $form; ?>

         </div> <!-- .<?php echo $class; ?>-copy-form -->

      </div>

     </div> <!-- .<?php echo $class; ?>-copy -->

     <?php if ($imagen): ?>
     <div class="<?php echo $class; ?>-graphic graphic">
        <div class="<?php echo $class; ?>-graphic-holder graphic-holder">
           <?php echo ma_image_lazy_responsive($imagen, 'mobile, medium', true); ?>
         </div>
     </div>
     <?php endif; ?>

 </div>
 <!-- /.home-form-holder -->