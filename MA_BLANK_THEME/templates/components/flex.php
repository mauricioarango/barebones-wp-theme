<?php
/* - - - - - - - - - - - - - - - - - - - - -
    flex.php

    Loops through the flex  ACF

    $class  = string, class to use
    $sizeFull = string, sizes for full bleed sections 'size1, size2'
    $sizeMed = string, sizes for medium size sections 'size1, size2'
   - - - - - - - - - - - - - - - - - - - - */
 ?>
 <?php
 if ( have_rows( 'flex_contents' ) ):
    while ( have_rows( 'flex_contents' ) ) : the_row();

        #
        # UPLOADED VIDEO OR CAROUSEL
        #
        if ( get_row_layout() == 'carrusel_o_video' ) : ?>

            <section class="<?php echo $class;?>--hero flex--hero">

                <div class='section-holder section-holder--fullBleed'>

                    <?php

                    $choice = get_sub_field( 'slideshow_o_video_selection' );
                    $choice_images = get_sub_field( 'slideshow_o_video_slideshow' );
                    $choice_video = get_sub_field( 'slideshow_o_video_video' );

                    if ( $choice ==  'carrusel' && !empty($choice_images)  ) :
                        # carousel, slideshow
                        silencio_partial('/templates/components/slideshow', ['images' => $choice_images, 'class'=>'slideshow--fullBleed slideshow--withEyelets slideshow--auto', 'sizes' => $sizeFull]);
                    elseif ( $choice == 'video' && !empty($choice_video) ):
                         # uploaded video
                         silencio_partial('/templates/components/video', ['videoID' => $choice_video, 'class'=>'video--fullBleed', 'attributes'=> 'autoplay playsinline loop muted ']);
                    endif;

                    silencio_partial('/templates/components/scrollDown');

                    ?>

                </div> <!-- .section-holder section-holder--fullBleed -->

            </section> <!-- ./<?php echo $class;?>--hero -->

       <?php

        #
        # PARALLAX OR FULL BLEED IMAGE
        #
        elseif ( get_row_layout() == 'imagen_de_ancho_a_ancho' ) :

            # figures if this is a parallax situation, and creates specific classes
            $parallax = false;
            if ( get_sub_field( 'paralax' ) == 1 ) :
                $parallax = true ;
                $classAside =$class.'-singleImage image--fullBleed image--parallax';
            else:
                $classAside =$class.'-singleImage image--fullBleed';
            endif;
            # gets image id
            $imagen_ancha = get_sub_field( 'imagen_ancha' );

            if ( $imagen_ancha ) :
                silencio_partial('/templates/components/imagenFullBleed', ['image' => $imagen_ancha, 'classAside'=>$classAside, 'parallax'=>$parallax, 'sizes' => $sizeFull]);
                ?>


       <?php endif; # end of parallax, full bleed section

        #
        # CARROUSEL (SLIDESHOW)
        #
        elseif ( get_row_layout() == 'carrusel' ) :
            $carrusel_ids = get_sub_field( 'carrusel' );
            if ( $carrusel_ids ) : ?>

            <section class="<?php echo $class; ?>-slideshow flex--carousel slideshow-holder--normal">

                <div class='section-holder'>

                <?php silencio_partial('/templates/components/slideshow', ['images' => $carrusel_ids, 'class'=>$class.'slideshow slideshow--normal', 'sizes' => $sizeMed.' '.$sizeFull]); ?>

                </div> <!-- .section-holder -->

            </section> <!--  /. <?php echo $class; ?>-slideshow -->
        <?php
            endif;

        #
        # VIDEO EMBED
        #
        elseif ( get_row_layout() == 'video_youtube_vimeo' ) : ?>

            <section class='<?php echo $class; ?>-embed flex--embed'>

                <div class='section-holder'>
                    <div class='<?php echo $class;?>-embed-media embedded-media'>
                        <?php echo wp_oembed_get( get_sub_field( 'url_del_video' ) ); ?>
                    </div> <!-- .<?php echo $class;?>-embed-media -->
                </div>

            </section> <!-- .<?php echo $class; ?>-embed -->

        <?php
        #
        # EDITORIAL: 1 IMAGE + text
        #
        elseif ( get_row_layout() == 'editorial' ) :
            $edit_contents = [];
            $edit_contents['imagen_a_la_izquierda'] = get_sub_field( 'imagen_a_la_izquierda' );
            $edit_contents['imagen'] = get_sub_field( 'imagen' );
            $edit_contents['titulo_principal'] = get_sub_field( 'titulo_principal' );
            $edit_contents['titulo_secundario'] = get_sub_field( 'titulo_secundario' );
            $edit_contents['texto_editorial'] = get_sub_field( 'texto_editorial' );
            $edit_contents['cita'] = get_sub_field( 'cita' );
            $edit_contents['autor_de_la_cita'] = get_sub_field( 'autor_de_la_cita' );

            $alignClass = $edit_contents['imagen_a_la_izquierda'] ? 'section-bg--left' : '' ;

        ?>

        <section class='<?php echo $class; ?>-editorialOne flex--editorialOne'>

            <div class="section-bg <?php echo $alignClass; ?>"></div>
            <div class='section-holder'>

                <?php
                    silencio_partial('/templates/components/textAndImage', [ 'row' => $edit_contents, 'sizes' => $sizeMed ]);
                ?>

            </div>


        </section> <!-- .<?php echo $class; ?>-editorialOne -->

        <?php
        #
        # EDITORIAL: 2 COLUMNS OF IMAGES
        #
        elseif ( get_row_layout() == 'editorial_2_columnas' ) :
            if ( have_rows( 'contenido_editorial_--_editorial_content' ) ) :
                while ( have_rows( 'contenido_editorial_--_editorial_content' ) ) :  $row = the_row(true); ?>

                    <section class='<?php echo $class; ?>-editorialTwo flex--editorialTwo'>

                        <div class="section-bg "></div>
                        <div class='section-holder'>

                            <?php
                                silencio_partial('/templates/components/textAndImage_2columns', [ 'row' => $row, 'sizes' => $sizeMed ]);
                            ?>

                        </div>

                    </section>

        <?php
                endwhile;
            endif;
        endif;
    endwhile;
 endif; ?>