<?php
/* - - - - - - - - - - - - - - - - - - - - -
    video.php

    master component for videos

    $videoID = number, video IDs
    $class  = string class to use
    attributes = string, list of attributes to append
   - - - - - - - - - - - - - - - - - - - - */
 ?>
 <!-- .video-container -->
 <div class="video-container <?php echo $class ?>">
    <?php echo ma_videoHTML5($videoID, $attributes) ?>
 </div>
 <!-- /.video-container -->