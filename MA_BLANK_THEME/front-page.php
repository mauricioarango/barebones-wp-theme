<?php
/* - - - - - - - - - - - - - - - - - - - - -
    front-page.php

    Template for the site's homepage
   - - - - - - - - - - - - - - - - - - - - */
?>
<?php get_header(); ?>
<?php

    if (have_posts()): while (have_posts()) : the_post();
    # sets loop so that we can access variables

        # uses silencio_partial() to load template parts, and pass variables to them
        # silencio_partial('/templates/home/home-header', ['inicio_ID' => $post->ID]);
        # silencio_partial('/templates/home/home-imageTop', ['inicio_ID' => $post->ID]);
        # silencio_partial('/templates/home/home-USintro', ['inicio_ID' => $post->ID]);

    endwhile; endif;
?>
<?php get_footer(); ?>