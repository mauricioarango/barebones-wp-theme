<?php
/*
 *  Author: Outset.org
 *  URL: outset.org
 *  Blank theme with support for menus, cpt, custom categories, thumb sizes
 */

/* - - - - - - - - - - - - - - - - - - - - -
	Theme Support
   - - - - - - - - - - - - - - - - - - - - */
if (function_exists('add_theme_support'))
{
    # Add Menu Support
    add_theme_support('menus');
    register_nav_menus(array( # Using array to specify more menus if needed
        'header-menu' => __('Header Menu'), # Main Navigation
        'footer-menu' => __('Footer Menu'), # Extra Navigation if needed
    ));

    # Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('mobile', 900, 900, false); // for use in mobile. creates an image with 900px at its max dimension

    # Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');  # move to header.php
}

/* - - - - - - - - - - - - - - - - - - - - -
    FILTERS & ACTIONS
- - - - - - - -  - - - - - - - - - - - - - - */

# Loads styles and Js files
function ma_enqueues()
{

    # Styles
    wp_register_style('ma_styles_custom', get_template_directory_uri() . '/dist/css/main.css', [], '1.0', 'all');
    wp_enqueue_style('ma_styles_custom'); # Enqueue it!

    # scripts
    // Loads conditional scripts
    // if (is_page('pagenamehere')) {
    //     wp_register_script('scriptname', get_template_directory_uri() . '/dist/js/scriptname.js', array(''), '', true); # Conditional script(s)
    //     wp_enqueue_script('scriptname'); # Enqueue it!
    // }
    wp_register_script('jsBundle', get_template_directory_uri() . '/dist/js/main.bundle.js', [], '1.0', true); # UX brains!
    wp_enqueue_script('jsBundle'); # Enqueue it!
}

# Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function ma_add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

/* - - - - - - - - - - - - - - - - - - - - -
	Actions + Filters + ShortCodes
   - - - - - - - - - - - - - - - - - - - - */

# Actions
add_action('wp_enqueue_scripts', 'ma_enqueues'); # Add Theme Stylesheet
# Add Filters
add_filter('body_class', 'ma_add_slug_to_body_class'); # Add slug to body class (Starkers build)

/* - - - - - - - - - - - - - - - - - - - - -
    REQUIRES:
    Needed files with generic custom functionality
    Common to most themes
   - - - - - - - - - - - - - - - - - - - - */
require_once get_template_directory() . '/inc/php/head_stuff.php';
require_once get_template_directory() . '/inc/php/media.php';
require_once get_template_directory() . '/inc/php/helper_functions.php';
require_once get_template_directory() . '/inc/php/admin_settup.php';
// require_once get_template_directory() . '/inc/php/custom_Post_Type_And_Archive.php'; # use it to create CPT and Custom Taxonomies without a plugin
// require_once get_template_directory() . '/inc/php/social_sharing.php';
// require_once get_template_directory() . '/inc/php/youtube_vimeo.php';
?>