const path = require("path");
const CopyPlugin = require("copy-webpack-plugin"); // for copying of folders, files
const MiniCssExtractPlugin = require("mini-css-extract-plugin"); // extracts and saves css files
// const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin"); // Minifies css
// const cssnano = require("cssnano"); // Used by OptimizeCSSAssetsPlugin  in the css minification
var webpack = require('webpack'); // used by SourceMapDevToolPlugin
const postcssPresetEnv = require('postcss-preset-env');

module.exports = {
    cache: false,
    entry: {
        'main': [ // forces webpack to process specified files
                 '../src/js/main.js',
                 '../src/sass/main.scss',
                 ],
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, '../dist/js'), // watchout: all output paths in 'rules' will be relative to this path
    },
    mode: 'development',
    devtool: false, // passes sourcemap control to SourceMapDevToolPlugin
    module:{
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets:
                        [
                            '@babel/env',
                            {
                                // "corejs": 3,
                            }
                        ]
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                        },

                        {
                            loader: 'css-loader', //The css-loader interprets @import and url() like import/require() and will resolve them.
                            options: {
                                url: false, // wont do anything to urls in css
                                sourceMap: true,
                            },
                        },

                        {
                            loader: 'postcss-loader',  // adds prefixes
                            options: {
                                sourceMap: true,
                            },
                        },

                        {
                            loader: 'sass-loader', // Loads a Sass/SCSS file and compiles it to CSS
                            options: {
                                sourceMap: true,
                                // importer: globImporter(),
                            },
                        },
                ]
            },

        ] // End of rules
    }, // End of module
    plugins: [
        new MiniCssExtractPlugin({
            filename: '../css/[name].css'
        }),

        new CopyPlugin([
            {
                from: '../src/fonts',
                to: '../fonts', // relative to output path
                toType: 'dir',
                ignore: ['*.zip'],
                force: true,
            },
            {
                from: '../src/images',
                to: '../images',
                toType: 'dir',
                ignore: ['*.zip'],
                force: true,
            },

        ]),
        // new OptimizeCSSAssetsPlugin({
        //   cssProcessor: cssnano,
        //   cssProcessorOptions:  {
        //           map: {
        //             inline: false,  // set to false if you want CSS source maps
        //             annotation: true
        //           },
        //           discardComments: {
        //             removeAll: true,
        //           },
        //           // Run cssnano in safe mode to avoid
        //           // potentially unsafe transformations.
        //           safe: true,
        //         },
        //   canPrint: false,
        // }),
        new webpack.SourceMapDevToolPlugin(
            {
                filename: '[file].map[query]',
                columns: false,
                 module: true,
            }
        ),
      ],
} // End of module.exports