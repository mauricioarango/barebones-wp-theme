/* - - - - - - - - - - - - - - - - - - - - -
    sets up all the carousels, slideshows, etc seen in the site
    keep as a separate file becasue there are so many!
   - - - - - - - - - - - - - - - - - - - - */

// SWIPER JS
// coudlnt make the npm work, so downloaded directly
import Swiper from './vendor/swiperjs/swiper.js' // css imported via main.scss

class swipeUX {
    // Properties\
    constructor() {

        this._tablet = getComputedStyle(document.documentElement).getPropertyValue('--query_tablet')
        this._medium = getComputedStyle(document.documentElement).getPropertyValue('--query_medium')
        this._laptop = getComputedStyle(document.documentElement).getPropertyValue('--query_laptop')
        this._xl = getComputedStyle(document.documentElement).getPropertyValue('--query_xl')
        this._xxl = getComputedStyle(document.documentElement).getPropertyValue('--query_xxl')

    }

    // Methods
    homeProjects() {
        const homeProjects = '.home-projects-slider .swiper-container'

        var mySwiper = new Swiper(homeProjects, {
            // Default parameters
            centeredSlides: true,
            grabCursor: true,
            loop: true,
            loopedSlides: 1,
            pagination: {
                el: null,
            },
            slidesPerView: 1,

            // Navigation arrows
            navigation: {
                prevEl: '.home-projects-slider-prev',
                nextEl: '.home-projects-slider-next',
            },

            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 761
                761: {
                    centeredSlides: false,
                    loopedSlides: 2,
                    slidesPerView: 2,
                },
                1140: {
                    centeredSlides: false,
                    loopedSlides: 3,
                    slidesPerView: 3,
                },
                1600: {
                    centeredSlides: false,
                    loopedSlides: 4,
                    slidesPerView: 4,
                }

            }

        })
    }

    //home architects and articles, work exactly the same
    homeArchitectsArticles = ()=> {

        // get all sliders that have same behavir
        var swipers = document.querySelectorAll(".homePage-offcenterSlider")
        // loops through each result and creates its slider
        var sliders = []

        swipers.forEach(
            (currentValue, currentIndex, listObj) => {
                const swiperID = currentValue.id
                const swiperTarget = '#' + swiperID
                const swiperPrev = '.' + swiperID + '-prev';
                const swiperNext = '.' + swiperID + '-next';

                sliders[swiperID] = new Swiper(swiperTarget, {
                    // Default parameters
                    centeredSlides: false,
                    loop: false,
                    loopedSlides: 2,
                    grabCursor: true,
                    pagination: {
                        el: null,
                    },
                    slidesPerView: 2,
                    spaceBetween: 8,
                    // Navigation arrows
                    navigation: {
                        prevEl: swiperPrev,
                        nextEl: swiperNext,
                    },

                    // Responsive breakpoints
                    breakpoints:{
                        760:{
                            loopedSlides: 'auto',
                            slidesPerView: 'auto',
                            spaceBetween: 25,
                            watchSlidesProgress: true,
                            watchSlidesVisibility: true,
                        }
                    },
                    breakpoints: {
                        // when window width is >= 761
                        1000: {
                            loopedSlides: 'auto',
                            slidesPerView: 'auto',
                            spaceBetween: 25,
                            watchSlidesProgress: true,
                            watchSlidesVisibility: true,
                        },
                    }

                })
                // Create an intersection observe to determine which slides are fully visible
                var slides = document.querySelectorAll(swiperTarget + ' .swiper-slide');
                this.observeSlides(document.querySelector(swiperTarget), slides)
            }

        )
    }

    observeSlides = (slideHolder, slides)=> {

        let options = {
            root: slideHolder,
            threshold: [0, 1]
        }

        let io = new IntersectionObserver(
            entries => {
                // Actions:
                entries.forEach(entry => {
                    if (entry.intersectionRatio == 1) {
                        entry.target.classList.add("slide--fullyOnview")
                    } else {
                        entry.target.classList.remove("slide--fullyOnview")
                    }
                })
            },
            options
        );

        slides.forEach(
            (currentValue)=>{
                io.observe(currentValue)
            }
        )

    }

    // Slideshows with pagination bullets
    withEyelets() {
        // get all sliders that have same behavir
        // Gets items that have the .slideshow--withEyelets class
        // and hero sliders on top of the page
        var swipers = document.querySelectorAll(".slideshow--withEyelets, .flex--hero .slideshow--fullBleed")

        var sliders = []
        Array.prototype.forEach.call(swipers, function(element) {
            const swiperID = element.id
            const swiperTarget = '#' + swiperID
            const swiperPrev = '.' + swiperID + '-prev';
            const swiperNext = '.' + swiperID + '-next';

            var autoValue
            if (element.classList.contains('slideshow--auto')){
                autoValue = true
            }else{
                autoValue = false;
            }

            sliders[swiperID] = new Swiper(swiperTarget, {
                autoplay: autoValue,
                grabCursor: true,
                loop: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                spaceBetween: 0,

                // Navigation arrows
                navigation: {
                    prevEl: null,
                    nextEl: null,
                },

                // Default parameters
                slidesPerView: 1,
                loopedSlides: 1,
                centeredSlides: true,
            })

            // Adds click to each slide so that they advance to the next
            const slides = element.getElementsByClassName('swiper-slide');
            Array.prototype.forEach.call(slides, function(slide) {
                slide.addEventListener('click', event => {
                    sliders[swiperID].slideNext();
                })
            })

        })
    }

    // 'normal' slideshow:
    // pagination, bullets, 1 slide at the time, loooped
    // Slideshows with pagination bullets
    normal = ()=> {
        // get all sliders that have same behavir
        var sliderObject = this // reference to the class
        var swipers = document.querySelectorAll(".slideshow--normal")
        if (swipers.length == 0) { return false}
        // loops through each result and creates its slider
        var sliders = []
        swipers.forEach(function(currentValue, currentIndex, listObj){
            const swiperID = currentValue.id
            const swiperTarget = '#' + swiperID

            const prev = document.querySelector('.' + swiperID + '-swiper-button-prev')
            const next = document.querySelector('.' + swiperID + '-swiper-button-next')

            sliders[swiperID] = new Swiper(swiperTarget, {

                grabCursor: true,
                loop: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                spaceBetween: 0,

                // Navigation arrows
                navigation: {
                    prevEl: prev,
                    nextEl: next,
                },

                // Default parameters
                slidesPerView: 1,
                loopedSlides: 1,
                centeredSlides: true,
            })

            // When the slider is clicked, it advances to next slide
            sliders[swiperID].on('click', function(e){
                this.slideNext();
            })

            if (swiperID == 'timeline'){
                sliderObject.timelineExtras(sliderObject, sliders[swiperID],'aboutHistory-nav-button--active')
                // detect changes in media queries and triggers renit of timeline slider
                sliderObject.newHeights(sliderObject,  sliders[swiperID] )
            }

        },'')
    }

    timelineExtras = (sliderObject, theSlider, theClass)=>{
        var buttonList = document.querySelectorAll('.aboutUSHistory-nav button')

        // adds active class to fisr element in list
        buttonList[0].classList.add(theClass)

        // adds click action to buttons
        buttonList.forEach((currentValue, currentIndex) =>{
            currentValue.addEventListener('click', event =>{
                theSlider.slideToLoop(currentIndex)
            }, sliderObject)
         })

        theSlider.on('slideChange', function(){
            var ind = theSlider.realIndex
            // removes active clas from all li
            sliderObject.removeClass(buttonList, theClass)
            // adds active class to matching button
            buttonList[ind].classList.add(theClass)
        })

    }

    // Listens for screen resizes,
    newHeights = (sliderObject, theSlider) => {
        var mqls = [ // list of window.matchMedia() queries -- start from bigger to small
            window.matchMedia("(min-width: " + sliderObject._xxl + ")"),
            window.matchMedia("(min-width: " + sliderObject._xl + ")"),
            window.matchMedia("(min-width: " + sliderObject._laptop + ")"),
            window.matchMedia("(min-width: " + sliderObject._medium + ")"),
            window.matchMedia("(min-width: " + sliderObject._tablet + ")"),
        ]

        var mediaqueryresponse = event =>{
            for (var i=0; i<mqls.length; i++){ // loop through queries
                // Respond if there's a match
                if (mqls[i].matches){
                    theSlider.update()
                    break
                }
            }
        }

        for (var i=0; i < mqls.length; i++){ // loop through queries
            mqls[i].addListener(mediaqueryresponse) // call handler function whenever the media query is triggered
        }

    }

    // Bare
    // Slideshow without buttons or navigation... used in amenities
    bare() {
        const bare = '.slideshow--bare'

        var bareSwiper = new Swiper(bare, {
            grabCursor: false,
            loop: true,

            pagination: {
                el: null,
            },

            // Navigation arrows
            navigation: {
                prevEl: null,
                nextEl: null,
            },

            // Default parameters
            centeredSlides: true,
            effect: 'fade',
            loopedSlides: 1,
            slidesPerView: 1,

        })


        // actions for the amenities list. Adds a click to each list item
        var list = document.querySelectorAll('.projectsSingle-amenities-listing li');
        var selfClass = this // reference to this class
        if (list.length > 0 ) {
            list.forEach(
                function(currentValue, currentIndex){
                    // wehn list item is clicked it moved slider to same index slide
                    currentValue.addEventListener('click', event =>{
                        bareSwiper.slideToLoop(currentIndex)
                    }, selfClass)
                }
            )
            list[0].classList.add('--active');
        }

        // Adds click to the slider. Advances to next slide
        bareSwiper.on('click', function(e){
            this.slideNext();
        })
        // Detects when a slide has changed and then adds to the matching LI in amenites an active class
        bareSwiper.on('slideChange', function(){
            var ind = bareSwiper.realIndex
            selfClass.removeClass(list, '--active') // removes active clas from all li
            list[ind].classList.add('--active')

        })
    }

    // Receives a nodelist, iterates over each element and removes the
    removeClass= (list, theClass)=>{
        list.forEach(
         (currentValue, currentIndex) =>{
            currentValue.classList.remove(theClass);
         }
        )
    }

}

let swipes = new swipeUX()
swipes.homeProjects()
swipes.homeArchitectsArticles()
swipes.withEyelets();
swipes.normal();
swipes.bare();