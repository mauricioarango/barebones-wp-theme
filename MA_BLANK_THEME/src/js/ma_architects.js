/* - - - - - - - - - - - - - - - - - - - - -
     ma_architects.js

     Controls all interactions in architects page
    - - - - - - - - - - - - - - - - - - - - */
import * as commons from './ma_utils.js';

class Architects {
    // Properties
    constructor() {
        this._filterButtons = document.querySelectorAll('.architects-origenBar-selector button')
        this._architects = document.querySelectorAll('.architects-loop-single')
        this._loopHolder = document.querySelector('.architects-loop .section-holder')
        this._btnClass = 'btnOrigen--active'
        this._archClass = 'origen--visible'
        this._archInnerClass = 'architects-single-inner'
        this._archSingleClass = 'architects-loop-single'
        this._loopClass = '.architects-loop'
        this._loop = document.querySelector(this._loopClass)
        this._archExpandedClass = 'architects-loop-single--expanded'
        this._expander = document.querySelector('.architects-loop-expander')
        this._expanderClass = 'architects-loop-expander'
        this._expanderExpandedClass = 'architects-loop-expander--expanded'
        this._toCloneClass = 'architects-loop-single-details'
        this._showButtonClass = 'architects-loop-single-preview-button'
        this._showButtonParentClass = 'architects-loop-single-preview-buttonHolder'
        this._closeSmallClasses = 'architects-loop-single-details-closeSmall'
        this._time = parseFloat(getComputedStyle(document.documentElement).getPropertyValue('--fadeArchitect'));
        this._scrollPointClass = 'architects-loop-single-preview-scrollHere'
        this._internationalButton = document.querySelector('.architects-origenBar-selector .origen--internacionales')

        // Queries at which number of columns in arch grid changes
        this._mob = getComputedStyle(document.documentElement).getPropertyValue('--query_mob')
        this._tablet = getComputedStyle(document.documentElement).getPropertyValue('--query_tablet')
        this._laptop = getComputedStyle(document.documentElement).getPropertyValue('--query_laptop')
        this._large = getComputedStyle(document.documentElement).getPropertyValue('--query_xl')
    }

    // Adds button behavior for the  architects by origen filter
    // filterOrigen = () => {
    //     var btn

    //     for (btn of this._filterButtons) {
    //         if (!btn) { continue }
    //         btn.addEventListener('click', (event) => {
    //             // If expander is on displayit's closed
    //             if (document.querySelector( '.' + this._archExpandedClass) ) {
    //                 // is open... Remove content and contract drawer
    //                 // triggers the close btn action
    //                 var closeBtn = this._expander.querySelector('.' + this._closeSmallClasses)
    //                 closeBtn.click()
    //             }

    //             var moi = event.currentTarget;
    //             var origenTarget = moi.getAttribute('data-filter')
    //             if (!moi.classList.contains(this._btnClass)) {
    //                 // this btn filter becomes active
    //                 moi.classList.add(this._btnClass)
    //                 // resets all other filter btns
    //                 this.resetActiveFilter(moi)
    //                 // shows all items with this target
    //                 this.showOrigen(origenTarget)
    //             } else {
    //                 // the btn already has the class, we will remove it and make all architects visible
    //                 moi.classList.remove(this._btnClass)
    //                 // all architects are made visible
    //                 this.showAll()
    //             }

    //         })
    //     }
    // }

    // // removes active class from all the  other buttons in arch. filter
    // resetActiveFilter(currentBtn) {
    //     var btn
    //     for (btn of this._filterButtons) {
    //         if (btn != currentBtn) {
    //             btn.classList.remove(this._btnClass)
    //         }
    //     }
    // }

    // // Takes care of hiding/showiong architects based on the origen tag
    // showOrigen = (origenTarget) => {
    //     var arch
    //     var ind = 1
    //     // loops through all the archs.
    //     for (arch of this._architects) {
    //         // hides the arch wihout the target class
    //         // & shows the arch with the class
    //         // console.log(origenTarget)
    //         if (arch.classList.contains(origenTarget)) {
    //             // has the target origen, will show up
    //             if (!arch.classList.contains(this._archClass)) {
    //                 // adds the visible class
    //                 arch.classList.add(this._archClass)
    //                 commons.showEl(arch, '.' + this._archInnerClass)
    //             }
    //             // adds index to data-index
    //             arch.setAttribute('data-index', ind)
    //             ind += 1

    //         } else {
    //             // do not have the target origen, will hide
    //             if (arch.classList.contains(this._archClass)) {
    //                 // adds the visible class
    //                 arch.classList.remove(this._archClass)
    //                 commons.hideEl(arch, 0.5)
    //             }

    //             arch.setAttribute('data-index', '-1')
    //         }


    //     }
    // }

    // // makes all archtitects visible
    // // Takes care of hiding/showiong architects based on the origen
    // showAll = () => {
    //     var arch
    //     var ind = 1
    //     // loops through all the archs.
    //     for (arch of this._architects) {
    //         arch.classList.add(this._archClass)
    //         commons.showEl(arch, '.' + this._archInnerClass, 0.5)
    //         arch.setAttribute('data-index', ind)
    //         ind += 1
    //     }
    // }

    // Adds actions for expanding / contracting arch info
    viewInfo = () => {
        var arch

        // loops through all the archs.
        for (arch of this._architects) {
            var inner = arch.querySelector('.' + this._archInnerClass);

            inner.addEventListener('click', event => {

                var preview = event.currentTarget
                var boxParent = preview.closest('.' + this._archSingleClass)

                // Detects if clicked preview box is open or not
                if (boxParent.classList.contains( this._archExpandedClass)) {
                    // it is open:
                    // Remove content and clsoe drawer and remove cloned conent
                    var closeBtn = this._expander.querySelector('.' + this._closeSmallClasses)
                    closeBtn.click()
                } else {

                    // is expander already open?:
                    if (this._expander.classList.contains(this._expanderExpandedClass)) {
                        // it is already open:
                        // closes drawer
                        var closeBtn = this._expander.querySelector('.' + this._closeSmallClasses)
                        closeBtn.click()
                        // opens the new contents .. there needs to be a delay so that the drawer is properly closed & emptied out
                        var delay = this._time * 1000 * 1.5
                        setTimeout(this.openExpander, delay, boxParent)

                    } else {
                        // not open ... needs to open the expander and place cloned content
                        this.openExpander(boxParent)
                    }
                }

            })
        }

    }

    // Opens the expander and places cloned content on it
    openExpander = (boxParent) =>{
        boxParent.classList.add(this._archExpandedClass)

        // scrolls to bottom of this preview box
        var scrollToItem = boxParent.querySelector( '.' + this._scrollPointClass )
        commons.smoothScrolling(scrollToItem)

        // Figures where to insert expander
        var positions = this.detectRow(boxParent)
        // index (1 based) of element after which it will be inserted
        var insertAt = positions.afterBox
        // finds element after which exp. will be inserted
        var insertAtEl = document.querySelectorAll('.' + this._archSingleClass + '[data-index="' + insertAt + '"]')
        // inserts expander
        console.log(positions, insertAt)
        console.log(insertAtEl)
        // moves expander to the proper place
        insertAtEl[0].insertAdjacentElement('afterend', this._expander)
        // clones and places into expander content that will be displayed
        var toClone = boxParent.querySelector('.' + this._toCloneClass)
        var clonedEl = toClone.cloneNode(true)
        var insideExp = this._expander.appendChild(clonedEl)
        insideExp.classList.add('tempHidden--restablished')
        insideExp.classList.add(this._toCloneClass + '--onView')

        // Hides the show button
        var btn = boxParent.querySelector('.' + this._showButtonClass)
        var btnParent = btn.closest('.' + this._showButtonParentClass)
        commons.hideEl(btnParent, 0.5);

        // Shows up expander
        console.log(insideExp)
        this._expander.classList.add(this._expanderExpandedClass)
        commons.expand(this._expander, insideExp.offsetHeight, this._time)

        // Functions for the close button
        var closeBtn = insideExp.querySelector('.' + this._closeSmallClasses)
        closeBtn.addEventListener('click', this.closeExpander)
    }

    // Close Expander... actions to close the expander
    closeExpander = (event) =>{
        // reference to the btn itself
        var curr = event.currentTarget
        // ref. to cloned content inside expander
        var clnContent = curr.closest('.' + this._toCloneClass)
        // ref to Preview Box
        var previewBox = document.querySelector('.' + this._archExpandedClass)
        console.log(curr, clnContent, previewBox)
        // removes active class from preview box
        previewBox.classList.remove(this._archExpandedClass)
        // Hides cloned content inside the expander box
        console.log(clnContent.classList)
        clnContent.classList.add(this._toCloneClass + '--toHide')
        clnContent.classList.remove(this._toCloneClass + '--onView')
        // Hides expanded box
        commons.hideEl(this._expander, this._time/2)
        // Shows again button in preview box
        var showBtn = previewBox.querySelector('.' + this._showButtonClass)
        var showBtnParent = showBtn.closest('.' + this._showButtonParentClass)
        console.log(showBtnParent, showBtn)
        commons.showEl(showBtnParent, '.' + this._showButtonClass, this._time)
        // Removes clonned content and cleans added styles in expander div
        setTimeout(this.cleanExpander, this._time * 1000, clnContent)
    }

    // Listens for screen resizes,
    // figures if there's an architects info on view,
    // and if it needs to be placed in a differnt row as a result of the resizing and breakpoint change
    // view explanation of matchMedia here http://www.javascriptkit.com/javatutors/matchmediamultiple.shtml
    resizing = () => {

        var mqls = [ // list of window.matchMedia() queries -- start from bigger to small
            window.matchMedia("(min-width: " + this._large + ")"),
            window.matchMedia("(min-width: " + this._laptop + ")"),
            window.matchMedia("(min-width: " + this._tablet + ")"),
            // window.matchMedia("(max-width: " + this._mob + ")"),
            window.matchMedia("(min-width: 0px)"),
        ]

        var mediaqueryresponse = event =>{
            for (var i=0; i<mqls.length; i++){ // loop through queries
                // Respond if there's a match
                if (mqls[i].matches){
                    // console.log (mqls[i].media) // indicates current media query for match
                    this.repositionExpanderRow()
                    break
                }
            }
        }

        for (var i=0; i < mqls.length; i++){ // loop through queries
            // mediaqueryresponse(mqls[i]) // call handler function explicitly at run time
            mqls[i].addListener(mediaqueryresponse) // call handler function whenever the media query is triggered
        }

    }

    // Called when a new media query is triggered
    // Decides whether the expander needs to be place in a differnt row
    repositionExpanderRow = () => {
        // Detects if expander is on view
        if ( ! this._expander.classList.contains(this._expanderExpandedClass)) {
            // no expander on view, can silently stop execution
            return false
        }

        // ... an expander row is visible.
        // find what archtect is associated with it
        var arch
        for (arch of this._architects) {
            if ( arch.classList.contains( this._archExpandedClass ) ){
                var newPos = this.detectRow(arch)

                var insertAt = newPos.afterBox
                // finds element after which exp. will be inserted
                var insertAtEl = document.querySelectorAll('.' + this._archSingleClass + '[data-index="' + insertAt + '"]')
                // moves expander to the proper place
                insertAtEl[0].insertAdjacentElement('afterend', this._expander)

                break
            }
        }
    }

    // Removes clonned content and cleans added styles in expander div
    cleanExpander = (clnContent) => {
        clnContent.remove()
        this._expander.removeAttribute("style")
        this._expander.classList.remove('tempHidden')
        this._expander.classList.remove(this._expanderExpandedClass)
        this._expander.removeAttribute('data-heightinitial')
        // sends expander to the end
        this._loopHolder.appendChild(this._expander)
        // Keep in mind that when you append an element that is already in the DOM, this element will be moved, not copied.
    }

    // Finds current row and position of an expanded element


    // Detects row where expanded contents should go
    detectRow = (box) => {

        var boxW = parseInt(getComputedStyle(box).width, 10)
        var loopW = parseInt(getComputedStyle(this._loop).width, 10)
        var columns = Math.round(loopW / boxW)
        // finds current pos of clicked item, its current row
        var currentPos = box.getAttribute("data-index")
        var currentRow = Math.ceil(currentPos / columns)
        // Finds where the new content needs to be added
        var nextRow = currentRow + 1
        var afterBox = currentRow * columns
        // if the last row is not full we need to find out last available box
        var onViewBoxesArr = document.querySelectorAll('.origen--visible')
        var onViewBoxes = onViewBoxesArr.length
        if (onViewBoxes < afterBox) { afterBox = onViewBoxes }

        console.log(box)
        console.log(currentPos, columns, currentRow)
        console.log(afterBox, nextRow)

        return {
            'afterBox': afterBox,
            'nextRow': nextRow
        }
    }

    // launches an architect if it is in the url parameter
    // Otherwise triggers the international view
    launch = () =>{
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const arch = urlParams.get('arquitecto')
        console.log (arch)

        if (arch){
            // checks if there's an entry for that architect
            var targetArch = document.querySelector('#' + arch)
            if (targetArch){
                // delays call 1s, to make sure page layout is already in place and therefore, all geometry calculations are correct
                setTimeout(function() { targetArch.click() }, 1000);

            }

        }
    }

}
if (document.body.classList.contains('post-type-archive-arquitectos')){
    let architects = new Architects()
    // architects.filterOrigen() // selects international or local architects
    architects.viewInfo()
    architects.resizing()
    architects.launch()
}