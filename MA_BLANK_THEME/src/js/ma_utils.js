// - POLYFILLS
import smoothscroll from '../../npm_webpack/node_modules/smoothscroll-polyfill'; // iOS doesn't do native smoothscroll behavior
smoothscroll.polyfill(); // kick off the polyfill!

/* - - - - - - - - - - - - - - - - - - - - -
    Commonly used funcionts
   - - - - - - - - - - - - - - - - - - - - */
   const fadeOut = (el, time = 1, smooth = true, displayStyle = 'none') => {
       if (smooth) {
           let opacity = window.getComputedStyle(el).getPropertyValue("opacity")
           let request

           const growRate = opacity / ( time * 60 )

           const animation = () => {
                opacity -= growRate
                el.style.opacity = opacity
                if (opacity <= 0) {
                   opacity = 0
                   el.style.opacity = opacity
                   cancelAnimationFrame(request)
                }
           }

           const rAf = () => {
                   request = requestAnimationFrame(rAf)
                   animation()
           }
           rAf()

       } else {
           el.style.opacity = 0
       }
   }

   const fadeIn = (el, time = 1, smooth = true, displayStyle = 'block') => {
       el.style.opacity = 0
       el.style.display = displayStyle
       if (smooth) {
           let opacity = 0
           let request
           const growRate = 1 / ( time * 60 )

           const animation = () => {
               opacity += growRate
               el.style.opacity = opacity
               if (opacity >= 1) {
                   opacity = 1
                   cancelAnimationFrame(request)
               }
           }

           const rAf = () => {
               request = requestAnimationFrame(rAf)
               animation()
           }
           rAf()

       } else {
           el.style.opacity = 1
       }
   }

// Contracts el's height so that it disappears from view
   const contraction = (el, time= 1, smooth = true) => {

       var currentHeight = el.offsetHeight // saves current height, just in case is needed later on
       el.setAttribute('data-heightInitial', currentHeight)
       el.style.maxHeight = currentHeight +'px' // start contracting from this value


       el.classList.remove('autoHeight')


       const growRate = currentHeight / ( time * 60 )

       if (smooth) {
           let heightValue = currentHeight
           let request

           const animation = () => {
               heightValue -= growRate
               if (heightValue <= 0) {
                   heightValue = 0
                   el.classList.remove('tempHidden--restablished')
                   el.classList.add('tempHidden')
                   el.style.maxHeight = 'none'
                   el.style.height = 0
                   el.style.minHeight = 0
                   cancelAnimationFrame(request)
               }else{
                  el.style.maxHeight = heightValue +'px'
                  el.style.minHeight = heightValue +'px'
                  el.style.height = heightValue +'px'
               }
           }

           const rAf = () => {
               request = requestAnimationFrame(rAf)
               animation()
           }
           rAf()

       } else {
           el.style.height = 0
       }

   }

   // animates to a min height vallue
   // el = element to expand
   // minHeightValue = min height to grow to -- without the px number
   // time value in seconds
   const expand = (el, minHeightValue, time = 1, smooth = true, displayStyle = 'block') => {

       el.classList.add('tempHidden--restablished')

       const heightInitial = el.getAttribute('data-heightInitial')
       var minHeight = 0
       var targetH = 0
       if (heightInitial == false || heightInitial === null || !heightInitial === ''){
         targetH = minHeightValue
       }else{
         targetH = heightInitial
       }

       el.style.display = displayStyle
       const growRate = targetH / ( time * 60 )

       if (smooth) {
           let minHeight = 0
           let request

           const animation = () => {

               minHeight += growRate
               el.style.minHeight = minHeight +'px'

               if (minHeight >= targetH) {
                   minHeight = targetH
                   el.style.minHeight = minHeight +'px'
                   cancelAnimationFrame(request)
                   // adds class to eliminiate the overflow: hidden (in case something is still hidden or page is resized and height adjusted)
                   el.classList.add('autoHeight')
               }
           }

           const rAf = () => {
               request = requestAnimationFrame(rAf)
               animation()
           }
           rAf()

       } else {
           el.style.minHeight = targetH+'px'
           el.classList.add('autoHeight')
       }
   }



    // Unveils a hidden element when a button is pressed
    const showElButton = (el, time = 1) => {
        // el is the element that is clicked to unveil certain content (heroContent)
        const targetContainerName = el.getAttribute('data-targetItem')
        const targetContainer = document.querySelector('.' + targetContainerName)
        // heroContent is wrapped in a div (targetContainer).
        const heroContent = targetContainer.getElementsByTagName('div')[0] // what needs to be shown
        // min heght targetContainer needs to expand to so that contents are visible
        const targetMinHeight = heroContent.offsetHeight
        // TargetContainer is removed from the page's flow, so before the unveiling it needs to be brought into place:


        fadeIn(targetContainer, time)
        expand(targetContainer, heroContent.offsetHeight, time, false )
    }

    // showEl
    // fades in and expands an element
    const showEl = (el, innerClass, time = 1) => {
        const  innerH = el.querySelector(innerClass).offsetHeight
        fadeIn(el, time)
        expand(el, innerH, time, false )
    }

    // fades out and collapses an element
    const hideEl = (el, time = 1) => {
        fadeOut(el, time)
        contraction(el, time )
    }

    // clicks to smooth scroll to an element
    // Note: this needs an added polyfill so that it works in iOS and safari
    const smoothScrolling = (target) => {
        // const scrollTarget = document.querySelector('.'+target);

        target.scrollIntoView({
          behavior: 'smooth'
        });

    }

    export {fadeOut, fadeIn, expand, contraction, showEl, hideEl, showElButton, smoothScrolling}