/* - - - - - - - - - - - - - - - - - - - - -
    IMPORTS
   - - - - - - - - - - - - - - - - - - - - */

// SCSS
import '../sass/main.scss'; // webpack separates scss files and creates a unique css file

// VENDORS:
/* EXAMPLES
    // - Lazysizes
    import '../../npm_webpack/node_modules/lazysizes/plugins/bgset/ls.bgset.js';
    import lazysizes from '../../npm_webpack/node_modules/lazysizes';
    // Fitvids --makes embedded videos responsive
    import fitvid from './vendor/fitvid/fitvid.js';
*/

// - OWN CUSTOM FILES
/*
    example
    import * as commons from './ma_utils.js';
    import swipeUX from './ma_swipe.js'; // deals with all the carousels ... this one imports directly swiperjs
*/


    class MainUX {
        // Properties\


        // Methods
        headerSetUp = () => {

        }

        // clicks on hamburger menu
        hamburger = () => {


        }


        // Enables lazy loading in bg images
        lazyBgEnabler = () => {
            // console.log('lazy Bg')

            document.addEventListener('lazybeforeunveil', function(e){
                // console.log(e)
                var bg = e.target.getAttribute('data-bg');
                if(bg){
                    e.target.style.backgroundImage = 'url(' + bg + ')'
                }
            });
        }


    }

   let main = new MainUX()

   // calls methods at will
   main.headerSetUp() // actions on header
   main.hamburger() // clicks on hamburger
   main.lazyBgEnabler() // enables lazy bg