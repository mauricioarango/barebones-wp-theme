<?php
/**
 * Index template
 *
 * Last stop on the template hierarchy.
 *
 * @package WordPress
 * @version 1.0
 */
get_header();
?>
    <section>
        <h1>Latest Posts</h1>
        <?php if (have_posts()) : ?>

          <?php while (have_posts()) : the_post(); ?>
            <?php get_template_part('loop'); ?>
          <?php endwhile; ?>

        <?php else : ?>
          No results found
        <?php endif; ?>
    </section>
<?php get_footer();