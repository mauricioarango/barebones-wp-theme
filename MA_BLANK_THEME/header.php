<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
        <meta charset="<?php bloginfo('charset'); ?>">
		<meta name="description" content="<?php bloginfo('description'); ?>">
        <?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
        <?php
            # variables
            # path to svg files
            $svgPath = get_stylesheet_directory()."/dist/images/svg/";
            # this is how they can be added:
            # echo file_get_contents($svgPath."logo_lg.svg");
            # others below ..
         ?>

        <a class="skip-main assistiveOnly" href="#main">Go to main content</a>
		<!-- wrapper -->
		<div class="wrapper">

			<!-- header -->
			<header class="headerMain clear">

					<!-- logo and link to home page -->
                    <?php # if home page logo is in h1 tags ?>
                    <?php if (is_front_page()) echo '<h1>'; ?>
					<div class="logotypes">
						<a href="<?php echo home_url(); ?>" title="<?php echo get_bloginfo( 'name' ); ?>">
							<!-- svg logos -->
                            <figure class="logotype">
                                <?php echo file_get_contents($svgPath."logo.svg"); ?>
                            </figure>
						</a>
					</div>
                    <?php if (is_front_page()) echo '</h1>'; ?>
					<!-- /logo -->

					<!-- nav desktop -->
					<nav class="nav">
						<?php ma_blank_nav('header-menu', 'nav-menu'); ?>
					</nav>
					<!-- /nav -->

                   <div class="headerMain-side">

                        <button class="hamburger" aria-label="<?php _e('Abrir y Cerrar Navigacion en mobiles','ma_custom_strings'); ?>">
                            <span class="hamburger-open" title="Open Navigation Menu"><?php echo file_get_contents($svgPath."menu_hamburger.svg"); ?></span>
                            <span class="hamburger-close" title="Close Navigation Menu"><?php echo file_get_contents($svgPath."menu_close.svg"); ?></span>
                        </button>
                    </div> <!-- /.headerMain-side -->

                    <!-- mobile nav dropdown -->
                    <div class="navMobile">

                        <div class="navMobile-holder" role="navigation" aria-label="Mobile Navigation">
                            <?php # ma_blank_nav('header-menu', 'navMobile-holder-menu'); ?>
                        </div>

                    </div>
                    <!-- /mobile nav dropdown -->

			</header>
			<!-- /header -->

            <?php
                # we use headerDecoy to detect when it is in or out of boudns and turn sticky on/off
            ?>
            <div class='headerDecoy'></div> <!-- .headerDecoy -->
    <!-- #main -->
    <main id="main">
        <section class='stickyMainHeader'></section> <!-- .stickyMainHeader -->